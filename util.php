<?php
    date_default_timezone_set("America/Sao_Paulo");
    
    class FormatUtil {
        function dataHora($ts){
            if($ts > 0){
                return date("d/m/Y H:i", strtotime(date('Y-m-d H:i', $ts)));
            } else {
                return '';
            }
        }
    }
    
    class KeyValueUtil {
        static function put($key, $value){
            (new FileCacheUtil("key_value", $key))->put($value);            
        }
        
        static function get($key){
            return (new FileCacheUtil("key_value", $key))->get();
        }
    }
    
	class FileCacheUtil {		
		private $baseDir;
		private $fileName;
		
		function __construct($cBaseDir, $cBaseName) {
			$this->baseDir = $cBaseDir;			
			$this->fileName = 'cache/'.$cBaseDir.'/'.$cBaseName.'.cache';
		}
		
		function put($conteudo){
			$conteudo = trim($conteudo);
			if($conteudo=="") return;
			if (!file_exists('cache/'.$this->baseDir)) {mkdir('cache/'.$this->baseDir, 0777, true);}			
			$fp = fopen($this->fileName, 'w');
			fwrite($fp, $conteudo);
			fclose($fp);
		}
		
		function get(){
		    if($this->exists()){
		        return file_get_contents($this->fileName);		        
		    } else {
		        return '';
		    }
		}
		
		function delete(){
			return unlink($this->fileName);
		}

		function exists(){
			return file_exists($this->fileName);
		}
	}	
	
	class LogUtil {
		function logAccess(){
		    $txtLog = date("Y-m-d H:i", strtotime(date('Y-m-d H:i')))." | IP: ".$_SERVER['REMOTE_ADDR']." | ".$_SERVER['HTTP_USER_AGENT']." | ".$_SERVER['QUERY_STRING'];		
		    (new LogUtil())->logFile("ACESSS", $txtLog);
		}
		
		function logFile($type, $txt){
		    $txtLog = date("Y-m-d H:i", strtotime(date('Y-m-d H:i')))." | ".$type." | ".$txt."\n";
		    if (!file_exists('log')) {mkdir('log', 0777, true);}
		    file_put_contents('log/'.date("Ym").'.log', $txtLog, FILE_APPEND);
		}
	}
	
	class EmailUtil {
	    private $origem = 'admin@rom.foxth.tk';
	    
	    function enviar($para, $assunto, $txt){	        
	        $headers = 'From: ' . $this->origem . "\r\n" . 'X-Mailer: PHP/' . phpversion();
            return mail($para, $assunto, $txt, $headers);
	    }
	    
	    function enviarHTML($para, $assunto, $txt){
	        $headers = 'MIME-Version: 1.0' . "\r\n" . 'Content-type: text/html; charset=iso-8859-1' . "\r\n" . 'From: ' . $this->origem . "\r\n" . 'X-Mailer: PHP/' . phpversion();
	        return mail($para, $assunto, $txt, $headers);
	    }
	}
	
	function getParam($paramName){
	    return isset($_GET[$paramName])?$_GET[$paramName]:'';;
	}
	
	function getJSON($json_url){
	    return json_decode(file_get_contents($json_url));
	}
	
	function getServerURL(){
	    return (stripos($_SERVER['SERVER_PROTOCOL'],'https')===0?'https://':'http://').$_SERVER['SERVER_NAME'].':'.$_SERVER['SERVER_PORT'];	
	}
	
	function logINFO($txt){
	    (new LogUtil())->logFile("INFO", $txt);
	}
?>