$(document).ready(function(){
	numeral.register('locale', 'pt-br', {
        delimiters: {thousands: '.', decimal: ','},
        abbreviations: {thousand: 'mil', million: 'milhoes', billion: 'b', trillion: 't'},
        currency: {symbol: 'R$'}
    });
	numeral.locale('pt-br');		
});

var api = {	
	url:function(list, order, rarity, category, endCategory, q){
		url = 'api.php?list='+(list?list:'')+'&order='+(order?order:'popularity')+'&rarity='+(rarity?rarity:'')+'&category='+(category?category:'')+'&endCategory='+(endCategory?endCategory:'')+'&q='+encodeURI((q?q:''));
		$("#url").val(url);
		return url;
	},

	listar: function (busca, callback) {
		if(busca.q==undefined) busca.q = '';		
		if(busca.category.indexOf('equipment_all') >=0){busca.category = '1025'; busca.endCategory = '1030';}
		if(busca.category.indexOf('cards_all') >=0){busca.category = '1010'; busca.endCategory = '1016';}
		if(busca.category.indexOf('headwear_all') >=0){busca.category = '1040'; busca.endCategory = '1044';}
		
		sistema.aguardar.mostrar();
		
		if(busca.category.indexOf('list_') >=0){
			s = busca.category.split('_');
			$.getJSON(api.url(s[1]), function(json) {				
				sistema.aguardar.esconder();
				callback(json);
			});
			
		} else {
			url = api.url('', busca.order, busca.rarity, busca.category, busca.endCategory, busca.q);

			$.getJSON(url, function(json) {
				l = new Array();
				for (var i = 0; i < json.length; i++) {
					//if(parseFloat(json[i].lastRecord.snapEnd) > 0 && parseFloat(json[i].lastRecord.snapEnd)*1000 < new Date().getTime()) continue;
					l.push(json[i]);
				}
				sistema.aguardar.esconder();
				callback(l);
			});	
		}

	},
	
	obterItem: function (nomeItem, server, callback) {
		if(!nomeItem || nomeItem.indexOf('+')>=0 || nomeItem.indexOf('<')>=0 || nomeItem.indexOf('(broken)')>=0){
			callback(null); 
			return;
		}

		url = 'https://api'+server+'.poporing.life/get_latest_price/'+api._obterNomeItem_PoporingLife(nomeItem);

		$.getJSON(url, function(json) {
			callback(json);
		});	
	},

	// {"timestamp":1582193850,"price":11392752,"stock":1,"snapBuyers":16,"snapEnd":1582198315}
	historico: function(idItem, callback) {
		url = 'https://poring.world/api/history?id='+idItem+'&type=recent'; 

		$.getJSON(url, function(json) {
			callback(json);
		});
	},
	
	_obterNomeItem_PoporingLife: function(item){
		item = item+"";
		item = item.toLowerCase();
		item = util.replaceAll(item,' ','_');
		item = util.replaceAll(item,'-','_');
		item = util.replaceAll(item,'’','');			
		item = item.replace('*','star_');
		item = item.replace('★','_star_');			
		item = item.replace('[1]','_1s');
		item = item.replace('[2]','_2s');
		return item;  
	},
	
	_save: function(table, pos, v, callback) {
		url = 'kvdb.php?t='+table+'&k=save-'+pos+'&v='+encodeURIComponent(v);
		$.get(url, function(data){if(callback)callback(data)});
	},
	
	_load: function(table, pos, callback) {
		url = 'kvdb.php?t='+table+'&k=save-'+pos;
		$.get(url, function(data){if(callback)callback(data)});
	},
	
	saveSearch: function(pos, v, callback) {
		api._save('save_search', pos, v, callback);		
	},
	
	loadSearch: function(pos, callback) {
		api._load('save_search', pos, callback);		
	}
}