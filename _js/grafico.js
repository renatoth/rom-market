$(document).ready(function(){
	// {"timestamp":1582193850,"price":11392752,"stock":1,"snapBuyers":16,"snapEnd":1582198315}
	api.historico(url.param('id'), function(dados){
		if(dados && dados.length > 0){
			grafico.criar(dados);
		}
	});
});

// http://www.chartjs.org/
var grafico = {
	criar: function(dados){
		opcoesGrafico = function(label, color){
			return {
				type: 'line',
				data: {
					datasets: [{
						label: label,
						pointRadius: 0,
						borderColor: color,
						borderWidth: 1
					}]
				},
				options: {
					responsive: true,		    	
					legend: {display: true}
				}
			}
		}

		myChartPrice = new Chart(document.getElementById('myChartPrice').getContext('2d'), opcoesGrafico('Price','green'));
		myChartVolume = new Chart(document.getElementById('myChartVolume').getContext('2d'), opcoesGrafico('Qtde.','blue'));		    

		labelGrafico = [], dadosGraficoPreco = [], dadosGraficoVolume = [];
		
		if(dados){
			for (var i = dados.length-1; i >= 0; i--){
				labelGrafico.push(formatar.dataHoraTS(dados[i].timestamp));
				
				dadosGraficoPreco.push(dados[i].price);
				dadosGraficoVolume.push(dados[i].stock);
			}
			
			myChartPrice.data.labels = labelGrafico;
			myChartPrice.data.datasets[0].data = dadosGraficoPreco;			
			myChartPrice.update();

			myChartVolume.data.labels = labelGrafico;
			myChartVolume.data.datasets[0].data = dadosGraficoVolume;			
			myChartVolume.update();
		} else {
			
		}
	}
}