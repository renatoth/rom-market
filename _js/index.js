var _cookieSave = 'formSaveFields';
var _itens = {};

$(document).ready(function(){	
	// recupera cookies
	formSalvo = cookie.read(_cookieSave);
	if(formSalvo && formSalvo != null && formSalvo != ''){
		formUtil.preencher($("#formBuscar"), JSON.parse(formSalvo));
	}

	atualizarTabela();

	$("#botaoRefresh").click(function(){		
		atualizarTabela();
	});

	$("#botaoHot").click(function(){
		$("input[name=q]").val('');
		$("select[name=category]").val('');
		$("select[name=order]").val('popularity');
		atualizarTabela();
	});

	$("#botaoCard").click(function(){
		$("input[name=q]").val('');
		$("select[name=category]").val('list_card');
		$("select[name=order]").val('price');
		atualizarTabela();
	});

	$("#botaoDesc").click(function(){
		$("input[name=q]").val('');
		$("select[name=category]").val('');
		$("select[name=order]").val('priceChange1d');
		atualizarTabela();
	});

	$("input[name=q]").change(function(){
		$("select[name=category]").val('');
		$("select[name=order]").val('price');
		atualizarTabela();
	});
	
	$("select[name=category], select[name=rarity], select[name=order], select[name=modo]").change(function(){
		atualizarTabela();
	});
	
	$("button[name=botaoLoad]").click(function(){				
		$("select[name=category]").val('list_save');
		$("select[name=order]").val('price');
		atualizarTabela();
	});	

	$("button[name=botaoCron]").click(function(){				
		$("select[name=category]").val('list_cron');
		$("select[name=order]").val('price');
		atualizarTabela();
	});
	
	$("select[name=selectSave]").change(function(){
		dadosForm = formUtil.obterJSON($("#formBuscar"));		
		slot = $(this).val();
		if(slot == 'c'){
			api.saveSearch('1', '*DELETE*', function(){
				api.saveSearch('2', '*DELETE*', function(){
					api.saveSearch('3', '*DELETE*', function(){
						alert('Cleared!');
					});
				});
			});
		} else {
			api.saveSearch(slot, dadosForm.url, function(){
				alert('Saved!');
			});
		}
	});
});

function atualizarTabela(){
	$("select[name=selectSave]").val('');
	$("#painelItens").html('');
	
	dadosForm = formUtil.obterJSON($("#formBuscar"));

	cookie.create(_cookieSave, JSON.stringify(dadosForm));

	api.listar(dadosForm, function(lista){
		_itens = {};
		if(lista.length == 0){
			$("#painelItens").html('<p> <span class="glyphicon glyphicon-remove" aria-hidden="true"></span> No records found...</p>');
		} else {
			modoView = $('select[name=modo]').val();			
			for (var i = 0; i < lista.length; i++) {
				if(modoView=='table'){
					montarTabela(lista[i], i+1, lista.length);
				} else if(modoView=='mini'){
					montarTabelaMini(lista[i], i+1, lista.length, dadosForm.filtroSort);				
				}
				_itens[lista[i].id] = lista[i];
			}
		}
	});
}

function montarTabela(item, seq, total){	
	openRow = (seq == 1);
	closeRow = (seq == total);

	nomeItem = item.name;
	nomeItem = nomeItem.replace('<','&lt;');
	nomeItem = nomeItem.replace('>','&gt;');

	corLinha = 'gray';
	if(item.rarity == 2) corLinha = 'green';
	if(item.rarity == 3) corLinha = 'blue';
	if(item.rarity == 4) corLinha = 'purple';

	if(openRow){
		$("#painelItens").append(
			'<div class="col-md-12">'+
			'<table class="table table-striped">'+
			'	<thead>'+
			'		<tr><th></th><th>Item</th><th>Category</th><th>Price</th><th>Qtd.</th><th>Snap</th><th>%1d</th><th>%3d</th><th>%7d</th></tr>'+
			'	</thead>'+
			'	<tbody id="tbodyItens"></tbody>'+
			'</table>'+
			'</div>'
		);
	}

	leilao = '-';
	if(item.lastRecord && item.lastRecord.snapBuyers > 0){
		if((item.lastRecord.snapEnd*1000) < new Date().getTime()){ // terminou
			leilao = '<span class="glyphicon glyphicon-ban-circle" aria-hidden="true"></span>';
		} else {
			leilao = '<span class="glyphicon glyphicon-user" aria-hidden="true"></span> ' + formatar.numero0(item.lastRecord.snapBuyers)
				+' <span class="glyphicon glyphicon-time" aria-hidden="true"></span> '+formatar.numero0(((item.lastRecord.snapEnd*1000)-new Date().getTime())/1000/60)+' min.';				
		}
	}
	
	price = formatar.numero0(item.lastRecord.price);
	if($("select[name=category]").val()=='list_card'){		
		gdPrice = 0;
		if(item.rarity == 1) gdPrice = parseFloat(item.lastRecord.price)/10; 
		if(item.rarity == 2) gdPrice = parseFloat(item.lastRecord.price)/20;
		if(item.rarity == 3) gdPrice = parseFloat(item.lastRecord.price)/50;		
		
		price = "<span class='label label-primary'>GD: " + formatar.numero0(gdPrice) + "</span> " + price;
	}
	
	$("#tbodyItens").append(
		'<tr data-id="'+item.id+'">'+
		'	<td width="40"><img src="https://poring.world/sprites/'+item.icon+'.png" style="max-width:30px; max-height:30px;"></td>'+
		'	<td><a href="#" style="color:'+corLinha+'" data-id="'+item.id+'">'+nomeItem+'</a></td>'+
		'	<td>'+item.category+'</td>'+		
		'	<td align=right>'+price+'</td>'+
		'	<td align=right>'+formatar.numero0(item.lastRecord.stock)+'</td>'+
		'	<td align=right>'+leilao+'</td>'+
		'	<td align=right>'+(parseFloat(item.priceChange1d)<0?"<font color=red>":"<font>")+formatar.percentual0(item.priceChange1d/100)+'</font></td>'+
		'	<td align=right>'+(parseFloat(item.priceChange3d)<0?"<font color=red>":"<font>")+formatar.percentual0(item.priceChange3d/100)+'</font></td>'+
		'	<td align=right>'+(parseFloat(item.priceChange7d)<0?"<font color=red>":"<font>")+formatar.percentual0(item.priceChange7d/100)+'</font></td>'+		
		'</tr>'
	);
	
	_montarLinkGrafico(item);	
}

function montarTabelaMini(item, seq, total){	
	openRow = (seq == 1);
	closeRow = (seq == total);

	nomeItem = item.name;
	nomeItem = nomeItem.replace('<','&lt;');
	nomeItem = nomeItem.replace('>','&gt;');
	
	corLinha = 'gray';
	if(item.rarity == 2) corLinha = 'green';
	if(item.rarity == 3) corLinha = 'blue';
	if(item.rarity == 4) corLinha = 'purple';

	filtroSort = $("select[name=order]").val();
	nomeColuna = '%7d';
	valorColuna = item.priceChange7d/100;
	
	if(filtroSort=='priceChange1d'){
		nomeColuna = '%1d';
		valorColuna = item.priceChange1d/100;
	}
	else if(filtroSort=='priceChange3d'){
		nomeColuna = '%3d';
		valorColuna = item.priceChange3d/100;
	}
	
	if(openRow){
		$("#painelItens").append(
			'<div class="col-md-12">'+
			'<table class="table table-striped">'+
			'	<thead>'+
			'		<tr><th></th><th>Item</th><th>Price</th><th>'+nomeColuna+'</th></tr>'+
			'	</thead>'+
			'	<tbody id="tbodyItens"></tbody>'+
			'</table>'+
			'</div>'
		);
	}

	leilao = '';
	if(item.lastRecord && item.lastRecord.snapBuyers > 0){
		if((item.lastRecord.snapEnd*1000) < new Date().getTime()){ // terminou
			leilao = '<span class="glyphicon glyphicon-ban-circle" aria-hidden="true"></span> ';
		} else {
			leilao = '<span class="glyphicon glyphicon-fire" aria-hidden="true"></span> ';
		}
	}

	$("#tbodyItens").append(
		'<tr data-id="'+item.id+'">'+
		'	<td width="40"><img src="https://poring.world/sprites/'+item.icon+'.png" style="max-width:30px; max-height:30px;"></td>'+
		'	<td><a href="#" style="color:'+corLinha+'" data-id="'+item.id+'">'+nomeItem+'</a></td>'+
		'	<td align=right nowrap>'+leilao+formatar.numero0(item.lastRecord.price)+'</td>'+
		'	<td align=right>'+(parseFloat(valorColuna)<0?"<font color=red>":"<font>")+formatar.percentual0(valorColuna)+'</font></td>'+		
		'</tr>'
	);

	_montarLinkGrafico(item);	
}

function _montarLinkGrafico(item){
	$('a[data-id="'+item.id+'"]').click(function(){		
		id = $(this).attr('data-id');
				
		corLinha = 'gray';
		if(item.rarity == 2) corLinha = 'green';
		if(item.rarity == 3) corLinha = 'blue';
		if(item.rarity == 4) corLinha = 'purple';
	
		nomeItem = item.name;
		nomeItem = nomeItem.replace('<','&lt;');
		nomeItem = nomeItem.replace('>','&gt;');
	
		descricao = '';
		if(item.cardEffect) {
			descricao += item.cardEffect;
		} 
		if(item.description) {
			descricao += item.description;
		}
		if(item.equipEffect){
			equipEffect = JSON.stringify(item.equipEffect).replace('{','').replace('}','');
			equipEffect = util.replaceAll(equipEffect, '\"', '');
			descricao += '<br>' + equipEffect;
		}
		if(item.equipUniqueEffect) {
			descricao += '<br>' + item.equipUniqueEffect;
		}
		if(item.depositReward){
			descricao += '<br><br>Deposit: ' + item.depositReward;
		}
		if(item.unlockReward){
			descricao += '<br>Unlock: ' + item.unlockReward;
		}

		leilao = '-';
		if(item.lastRecord && item.lastRecord.snapBuyers > 0){
			if((item.lastRecord.snapEnd*1000) < new Date().getTime()){ // terminou
				leilao = '<span class="glyphicon glyphicon-ban-circle" aria-hidden="true"></span>';
			} else {
				leilao = '<span class="glyphicon glyphicon-user" aria-hidden="true"></span> ' + formatar.numero0(item.lastRecord.snapBuyers)
					+' <span class="glyphicon glyphicon-time" aria-hidden="true"></span> '+formatar.numero0(((item.lastRecord.snapEnd*1000)-new Date().getTime())/1000/60)+' min.';				
			}
		}
	
		$('#divModalFullscreen').on('show.bs.modal', function () {
	    	$('#itemImg').attr("src", 'https://poring.world/sprites/'+_itens[id].icon+'.png');	    	
	    	$('#itemCategory').html(_itens[id].category);
	    	$('#itemDescription').html(descricao);
	    	$('#itemPrice').html(formatar.numero0(_itens[id].lastRecord.price));
	    	$("#itemPriceSEA").html("-");
			$('#itemQtde').html(formatar.numero0(_itens[id].lastRecord.stock));
			$('#itemSnap').html(leilao);

			$('#tableCraft tbody').html('');
			if(_itens[id].blueprintMaterials){
				sumCraft = 0;
				$.each(_itens[id].blueprintMaterials, function(index, value) {
					$('#tableCraft tbody').append('<tr><td>'+value.name+'</td><td align="right">'+formatar.numero0(value.quantity)+'</td><td align="right">'+formatar.numero0(value.unitPrice)+'</td><td align="right">'+formatar.numero0(parseFloat(value.quantity)*parseFloat(value.unitPrice))+'</td></tr>');
					sumCraft += parseFloat(value.quantity)*parseFloat(value.unitPrice);
				});
				$('#tableCraft tbody').append('<tr><td colspan=3 td align="right"><strong>Crafting Total:</strong></td><td align="right">'+formatar.numero0(sumCraft)+'</td></tr>');
				$('#tableCraft').show();
			} else{
				$('#tableCraft').hide();
			}

			$('#divModalIframe').attr("src", 'grafico.html?item='+_itens[id].name+'&id='+_itens[id].id);
	    	$('#divModalTitulo').html(nomeItem);
	    	
	    	_obterDadosSEA(_itens[id]);
		});
	    
	    $('#divModalFullscreen').modal({show:true})		
	});
}

function _obterDadosSEA(item){	
	api.obterItem(item.name, '', function(itemSEA){
		if(itemSEA && itemSEA.data && itemSEA.data.data && itemSEA.data.data.price > 0){			
			difSEA = 1-(parseFloat(itemSEA.data.data.price)/parseFloat(item.lastRecord.price));
			
			icone = "";
			if(parseFloat(item.lastRecord.price)*1.5 < parseFloat(itemSEA.data.data.price)){ // 50% mais barato
				icone = '<span class="glyphicon glyphicon-alert" aria-hidden="true"></span> ';
			} else if(parseFloat(item.lastRecord.price) > parseFloat(itemSEA.data.data.price)*1.5){ // 50% mais caro
				icone = '<span class="glyphicon glyphicon-alert" aria-hidden="true"></span> ';
			}
			
			$("#itemPriceSEA").html(icone + (difSEA<0?"<font color=red>":"<font>")+formatar.numero0(itemSEA.data.data.price) + ' (' + formatar.percentual0(difSEA)+')</font>');									
		}
	});
}