var MES = ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"];
var DIA = ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'];

var include = {
	js: function(src, useCache) {
		$('head').append($('<script>').attr('type', 'text/javascript').attr('src', src + (useCache?'':'?nc=' + $.now())));
	},
	css: function(src, useCache) {
		$('head').append($('<link>').attr('type', 'text/css').attr('rel', 'stylesheet').attr('href', src + (useCache?'':'?nc=' + $.now())));
	}
}

var formatar = {
	percentual: function(valor){
		if(valor==undefined) valor = 0;
		return numeral((valor*100).toString().replace(".",",")).format('0,0.00') + "%";
	},
	percentual1: function(valor){
		if(valor==undefined) valor = 0;
		return numeral((valor*100).toString().replace(".",",")).format('0,0.0') + "%";
	},
	percentual0: function(valor){
		if(valor==undefined) valor = 0;
		return numeral((valor*100).toString().replace(".",",")).format('0,0') + "%";
	},
	numero0: function(valor){
		if(valor==undefined) valor = 0;
		return numeral(valor.toString().replace(".",",")).format('0,0');
	},
	numero2: function(valor){
		if(valor==undefined) valor = 0;
		return numeral(valor.toString().replace(".",",")).format('0,0.00');
	},
	numero4: function(valor){
		if(valor==undefined) valor = 0;
		return numeral(valor.toString().replace(".",",")).format('0,0.0000');
	},
	dinheiro: function(valor){
		if(valor==undefined) valor = 0;
		return numeral(valor.toString().replace(".",",")).format('0,0.00');
	},
	horaUnix: function(data){
		var fmt2 = function(n){return n < 10 ? '0' + n : n;}		
		return fmt2(new Date(data * 1000).getHours()) 
			+ ':' + fmt2(new Date(data * 1000).getMinutes())
			+ ':' + fmt2(new Date(data * 1000).getSeconds());
	},	
	dataHoraTS: function(data){
		if(data==undefined || data==0) return "";		
		var fmt2 = function(n){return n < 10 ? '0' + n : n;}
		var d = new Date(data * 1000);
		return fmt2(d.getDate()) + '/' + (d.getMonth()+1) + '/' + d.getFullYear() + ' ' + d.getHours() + ':' + fmt2(d.getMinutes());
	},
	horaTS: function(data){
		if(data==undefined || data==0) return "";		
		var fmt2 = function(n){return n < 10 ? '0' + n : n;}
		var d = new Date(data * 1000);
		return d.getHours() + ':' + fmt2(d.getMinutes());
	},
	data: function(d){
		var fmt2 = function(n){return n < 10 ? '0' + n : n;}
		return fmt2(d.getDate()) + '/' + fmt2(d.getMonth()+1) + '/' + d.getFullYear();
	}
}

var url = {		
	params: function () {
		var match,
		pl     = /\+/g,  // Regex for replacing addition symbol with a space
		search = /([^&=]+)=?([^&]*)/g,
		decode = function (s) { return decodeURIComponent(s.replace(pl, " ")); },
		query  = window.location.search.substring(1);
		urlParams = {};
		while (match = search.exec(query))
		   urlParams[decode(match[1])] = decode(match[2]);			
		return urlParams;
	},
	
	param : function (nomeParametro) {
		var results = new RegExp('[\\?&]' + nomeParametro + '=([^&#]*)').exec(window.location.href);
		if (!results) {return '';}
		return url.decode(results[1]) || 0;
	},

	// public method for URL encoding
	encode : function (string) {
		return escape(this._utf8_encode(string));
	},

	// public method for URL decoding
	decode : function (string) {
		return this._utf8_decode(unescape(string));
	},

	// private method for UTF-8 encoding
	_utf8_encode : function (string) {
		string = string.replace(/\r\n/g,"\n");
		var utftext = "";

		for (var n = 0; n < string.length; n++) {
			var c = string.charCodeAt(n);
			if (c < 128) {
				utftext += String.fromCharCode(c);
			} else if((c > 127) && (c < 2048)) {
				utftext += String.fromCharCode((c >> 6) | 192);
				utftext += String.fromCharCode((c & 63) | 128);
			} else {
				utftext += String.fromCharCode((c >> 12) | 224);
				utftext += String.fromCharCode(((c >> 6) & 63) | 128);
				utftext += String.fromCharCode((c & 63) | 128);
			}
		}

		return utftext;
	},

	// private method for UTF-8 decoding
	_utf8_decode : function (utftext) {
		var string = "";
		var i = 0;
		var c = c1 = c2 = 0;

		while ( i < utftext.length ) {
			c = utftext.charCodeAt(i);
			if (c < 128) {
				string += String.fromCharCode(c);
				i++;
			} else if((c > 191) && (c < 224)) {
				c2 = utftext.charCodeAt(i+1);
				string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
				i += 2;
			} else {
				c2 = utftext.charCodeAt(i+1);
				c3 = utftext.charCodeAt(i+2);
				string += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
				i += 3;
			}
		}
		return string;
	}
}

var util = {
	obterHMS: function(){
		var d = new Date();
		var fmt2 = function(n){return n < 10 ? '0' + n : n;}
		var h = fmt2(d.getHours());
	    var m = fmt2(d.getMinutes());
	    var s = fmt2(d.getSeconds());
	    return h + ":" + m + ":" + s;
	},
	dataFutura: function(data){
		return (new Date(data.split('/').reverse().join('/')) > new Date());
	},
	somarData: function(data, dias){
		var result = new Date(data);
		result.setDate(result.getDate() + dias);
		return result;
	},
	proximaSexta: function(data){ 
		if(data.getDay()==5) return util.somarData(data, 7);
		if(data.getDay()==6) return util.somarData(data, 6);
		if(data.getDay()==0) return util.somarData(data, 5);
		if(data.getDay()==1) return util.somarData(data, 4);
		if(data.getDay()==2) return util.somarData(data, 3);
		if(data.getDay()==3) return util.somarData(data, 2);
		if(data.getDay()==4) return util.somarData(data, 1);
	},
	replaceAll: function(str, find, replace) {
		return str.replace(new RegExp(find, 'g'), replace);
	}
}


var formUtil = {
	obterJSON: function(elemento){
		var values = {};
		elemento.find(":input[name][type!='button'][type!='reset'][type!='submit'],span[data-name]").each(function(index, element) {
			var name = $(element).is("span") ? $(element).data("name") : $(element).attr("name");
			var tipo = $(element).data("tipo");
			if (name) {
				var value = undefined;
				if ($(element).is(":checkbox,:radio") && $(element).is(":checked")) {
					value = $(element).val();
				} else if ($(element).is("select")) {
					var option = $(element).find("option:selected");
					value = option.val() || "";
					var n = option.attr("name");
					if(n) {
						var v = option.text();
						if(value) {
							values[n] = v || "";
						}
					}
				} else if ($(element).is("input,textarea")) {
					value = $(element).val() || "";				
				} else if ($(element).is("span")) {
					value = $(element).html() || "";
				}
				if (value !== undefined) {
					if(tipo == 'Date'){
						value = desformatar.data(value);
					}
					else if(tipo == 'double'){
						value = desformatar.numero(value);
					}
					
					if(values[name] == null){
						values[name] = value;
					} else if ($(element).is(":checkbox")){
						if(!$.isArray(values[name])){
							var array = new Array();
							array.push(values[name]);
							values[name] = array;
						}
						values[name].push(value);
					}
				}
			}
		});
		return values;
	},
	
	preencher: function(elemento,bean){
		elemento.find(":input[name][type!='button'][type!='reset'][type!='submit'],span[data-name]").each(function(index, element) {
			var name = $(element).is("span") ? $(element).data("name") : $(element).attr("name");
			var tipo = $(element).data("tipo");
			var valor = bean[name];
			if (name) {				
				if ($(element).is(":checkbox,:radio")) {
					if (valor != undefined && valor == $(element).val()) {
						$(element).prop("checked", true);
					}
				}  
				else if ($(element).is("select")) {
					$(element).val(valor);
					
				} 
				else if ($(element).is(":hidden,:text,textarea")) {
					if(tipo=='Date'){
						valor = formatar.dataHora(valor);
					}					
					$(element).val(valor);
					
				} 
				else if ($(element).is("span")) {
					if (valor != undefined) {
						$(element).html(valor);
					}
				}
			}
		});
	},
	
	montarCombo: function(params) {
		var _select = params.objeto;
		
		_select.html('');
		_select.append($("<option>").text('carregando...'));
		
		params.callback = function(data){
			_select.html('');
			
			if(params.primeiroItem){
				_select.append($("<option>").attr("value","").text(params.primeiroItem));
			}
			
			$.each(data, function(k, v) {
				var vF = v;
				if(params.formatar){vF = params.formatar(v);}
				_select.append($("<option>").attr("value",vF.id).text(vF.nome));
			});
		};
				
		ajax.get(params);
		return _select;
	}
}


// funcoes de sistema
/**
 * Module for displaying "Waiting for..." dialog using Bootstrap
 * https://bootsnipp.com/snippets/featured/quotwaiting-forquot-modal-dialog
 * @author Eugene Maslovich <ehpc@em42.ru>
 */
var waitingDialog = waitingDialog || (function ($) {
    'use strict';

	// Creating modal dialog's DOM
	var $dialog = $(
		'<div class="modal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true" style="padding-top:15%; overflow-y:visible;">' +
		'<div class="modal-dialog modal-m">' +
		'<div class="modal-content">' +
			'<div class="modal-header"><h4 style="margin:0;"></h4></div>' +
			'<div class="modal-body">' +
				'<div class="progress progress-striped active" style="margin-bottom:0;"><div class="progress-bar" style="width: 100%"></div></div>' +
			'</div>' +
		'</div></div></div>');

	return {
		/**
		 * Opens our dialog
		 * @param message Custom message
		 * @param options Custom options:
		 * 				  options.dialogSize - bootstrap postfix for dialog size, e.g. "sm", "m";
		 * 				  options.progressType - bootstrap postfix for progress bar type, e.g. "success", "warning".
		 */
		show: function (message, options) {
			// Assigning defaults
			if (typeof options === 'undefined') {
				options = {};
			}
			if (typeof message === 'undefined') {
				message = 'Loading ...';
			}
			var settings = $.extend({
				dialogSize: 'sm',
				progressType: '',
				onHide: null // This callback runs after the dialog was hidden
			}, options);

			// Configuring dialog
			$dialog.find('.modal-dialog').attr('class', 'modal-dialog').addClass('modal-' + settings.dialogSize);
			$dialog.find('.progress-bar').attr('class', 'progress-bar');
			if (settings.progressType) {
				$dialog.find('.progress-bar').addClass('progress-bar-' + settings.progressType);
			}
			$dialog.find('h4').text(message);
			// Adding callbacks
			if (typeof settings.onHide === 'function') {
				$dialog.off('hidden.bs.modal').on('hidden.bs.modal', function (e) {
					settings.onHide.call($dialog);
				});
			}
			// Opening dialog
			$dialog.modal();
		},
		/**
		 * Closes dialog
		 */
		hide: function () {
			$dialog.modal('hide');
		}
	};
})(jQuery);


//manipulação de cookies
var cookie = {
	create: function (name,value,days) {
		if (days) {
				var date = new Date();
				date.setTime(date.getTime()+(days*24*60*60*1000));
				var expires = "; expires="+date.toGMTString();
		}
		else var expires = "";
		document.cookie = name+"="+value+expires+"; path=/";
	}
	,
	read: function (name) {
		var nameEQ = name + "=";
		var ca = document.cookie.split(';');
		for(var i=0;i < ca.length;i++) {
				var c = ca[i];
				while (c.charAt(0)==' ') c = c.substring(1,c.length);
				if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
		}
		return null;
	}
	,
	remove: function (name) {
		cookie.create(name,"",-1);
	}
}


var sistema = {
	redirecionar: function(pagina) {
		$(location).attr('href', 'pagina?p=' + pagina);
	},
	redirecionarR: function(pagina) {
		$(location).attr('href', 'paginaR?p=' + pagina);
	},
	spinner:{
		inserir: function(obj){obj.after(' <img id="img_loading" src="img/loading.gif"/>');	},
		remover: function(){$("#img_loading").remove();}
	},
	aguardar:{
		mostrar: function(){waitingDialog.show();},
		esconder: function(){waitingDialog.hide();}
	},
	esconder:{
		conteudo: function(){$("#divLayoutConteudo").hide()},
		modal: function(){if(window.parent) window.parent.$('#divModalFullscreen').modal('hide');}
	},
	mostrar:{
		conteudo: function(){$("#divLayoutConteudo").show()}		
	},
	debug:{
		ativar: function(){cookie.create("__debug","true")},
		ativado: function(){return cookie.read("__debug")=="true"},
		desativar: function(){cookie.remove("__debug")},
		alternar: function(){
			if(sistema.debug.ativado()){
				sistema.debug.desativar();
				logger.hide();
				sistema.notificar("Debug desativado.");
			} else {
				sistema.debug.ativar();
				logger.show();
				sistema.notificar("Debug ativado.");
			}
		}
	},
	_mensagem: function(msg, tipo, icone){
		if(tipo) $("#divMensagem").removeClass().addClass("alert alert-" + tipo);
		if(icone) $("#spanIcon").removeClass().addClass("glyphicon glyphicon-" + icone);
		
		$("#divMensagem").show(); 
		$("#spanMensagem").html(msg);		
	},
	mensagem: function(msg){sistema._mensagem(msg, 'success','ok')},
	mensagemAviso: function(msg){sistema._mensagem(msg, 'warning','exclamation-sign')},
	mensagemErro: function(msg){sistema._mensagem(msg, 'danger','remove')},
	_notificar: function(msg){$.notify({message:msg,icon:'glyphicon glyphicon-info-sign'},{type:'warning',placement:{from:'top',align:'center'}});},
	notificar: function(msg){sistema._notificar(msg);},
	confirmar: function(msg, funcaoConfirm, funcaoCancel){		
		bootbox.confirm({
		    title: "Confirmar Ação...",
		    message: msg,
		    buttons: {
		        cancel: {label: 'Cancelar'},
		        confirm: {label: '<span class="glyphicon glyphicon-ok"></span> Confirmar'}
		    },
		    callback: function (result) {
		    	if(result) 
		    		funcaoConfirm(); 
		    	else 
		    		if(funcaoCancel) funcaoCancel();
		    }
		})
	},
	modal: function(params){
	    $('#divModalFullscreen').on('show.bs.modal', function () {
	    	$('#divModalIframe').attr("src", params.src);
	    	if(params.title) $('#divModalTitulo').html(params.title);
		});
	    
	    if(params.onClose) $('#divModalFullscreen').on('hidden.bs.modal', params.onClose);
	    if(params.onLoad) $('#divModalFullscreen').on('shown.bs.modal', params.onLoad);
	    
	    $('#divModalFullscreen').modal({show:true})		
	}
}