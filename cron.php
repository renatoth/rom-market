<?php
    include 'util.php';
    
    header("Content-type: text/plain");
    
	// Use internal libxml errors -- turn on in production, off for debugging
	libxml_use_internal_errors(true);
	
	// log de acesso
	(new LogUtil())->logAccess();

	$jsonObj = getJSON(getServerURL().'/api.php?list=cron');
	
	$txtEmail = '';
	$count = 0;
	
	foreach ($jsonObj as $item) {
		$txtEmail = $txtEmail
			. $item->name
			. ' - Price: ' . number_format(floatval($item->lastRecord->price), 0, ',', '.')
			//. ' - Buyers: ' . $item->lastRecord->snapBuyers
			. ' - End: ' . (new FormatUtil())->dataHora(intval($item->lastRecord->snapEnd))
			. PHP_EOL;
		$count++;	    
	}
	
	if ($count == 0){
	    echo "Nenhum item encontrado!";
	    return;
	} 		
	
	// verifica se ja mandou o email		
	$txtHash = hash('sha256', $txtEmail);
	$cacheHash = KeyValueUtil::get('last-hash');
	if($txtHash == $cacheHash){
	    echo "Nada novo: " . $txtHash;
	    return;
	} else {
	    KeyValueUtil::put('last-hash', $txtHash);
	    KeyValueUtil::put('last-email', $txtEmail);	    
	}
	
	// envia o email
	if ($count == 1){
	    $subject = '[ROM] Cron | ' . $txtEmail;
	} else {
	    $subject = '[ROM] Cron | Itens encontrados';
	}
	
	(new EmailUtil())->enviar('renatoth@gmail.com', $subject, $txtEmail);

	echo $txtEmail;
?>