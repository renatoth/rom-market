<?php
    include 'util.php';
    
    header("Content-type: text/plain");
    
	// turn on in production, off for debugging
	libxml_use_internal_errors(true);
	
	// params
	$list          = getParam('list');
	$order         = getParam('order');
	$rarity        = getParam('rarity');
	$category      = getParam('category');
	$endCategory   = getParam('endCategory');
	$q             = getParam('q');
	
	$maxSize = 0; // limita o tamanho da resposta por url
	
	if($list == 'card'){
	    $maxSize = 5;
    	$urls = array(
    	    'https://poring.world/api/search?order=price&rarity=1&inStock=1&modified=&category=1010&endCategory=1016&q=',
    	    'https://poring.world/api/search?order=price&rarity=2&inStock=1&modified=&category=1010&endCategory=1016&q=',
    	    'https://poring.world/api/search?order=price&rarity=3&inStock=1&modified=&category=1010&endCategory=1016&q='
    	);
	} else if($list == 'cron'){
	    $urls = array(
	        'https://poring.world/api/search?order=price&rarity=&inStock=1&modified=&category=1027&endCategory=&q=tights%20[1]%20%3CMorale%204',
	        'https://poring.world/api/search?order=price&rarity=&inStock=1&modified=&category=&endCategory=&q=strength%20ring%20%5B1%5D%20%3CSharp%20Blade%202',
	        'https://poring.world/api/search?order=price&rarity=&inStock=1&modified=&category=&endCategory=&q=strength%20ring%20%5B1%5D%20%3CSharp%20Blade%203'
		);
	} else if($list == 'save'){
		$urls = array(
	        (new FileCacheUtil('save_search','save-1'))->get(),
			(new FileCacheUtil('save_search','save-2'))->get(),
			(new FileCacheUtil('save_search','save-3'))->get()
		);
	} else {
	    $urls = array(
	        'https://poring.world/api/search?inStock=1&modified=&order='.$order.'&rarity='.$rarity.'&category='.$category.'&endCategory='.$endCategory.'&q='.urlencode($q)	      
	    );
	}
	
	$mainArray = [];
	
	foreach($urls AS $url){
		if($url == '') break;
		$url = (stripos($url,'http')===0?'':getServerURL().'/').$url; // se for interno, completa com o http
		logINFO($url);
		$jsonObj = getJSON($url);
		if($jsonObj !== null){
			if($maxSize > 0) array_splice($jsonObj, $maxSize);    	
			$mainArray = array_merge($mainArray, $jsonObj);
		}
	}

	echo json_encode($mainArray, JSON_PRETTY_PRINT);
?>